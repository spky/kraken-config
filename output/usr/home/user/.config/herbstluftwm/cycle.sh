#!/usr/local/bin/bash
# shellcheck source=common.sh
. ~/.config/herbstluftwm/common.sh

cmd_launch() {
    name=$1; shift
    delay=$1; shift
    hc rename "+" "$name"
    env DISPLAY=:0 "$@" &
    pid=$!; sleep "$delay"; printf "launched %s\n" "$pid"
}
cmd_mplayer() { cmd_launch "$1" 20 mplayer -volume 0 -loop 0 -shuffle -playlist <(
    find "$2" -type f
); }
cmd_chrome() { cmd_launch "$1" 30 chrome \
    --disable-cloud-import --disable-infobars --disable-notifications \
    --disable-offline-auto-reload-visible-only \
    --hide-scrollbars --disable-overlay-scrollbar \
    --app="$2";
}

cleanup() {
    pkill mplayer; pkill chrome;
    reset_tags && printf "cleaned up\n"
}
append() {
    hc add "+"
    hc use_index +1
}

startup() {
    cleanup  || return

    cmd_chrome "bp" "http://localhost/baypack" || return
    append

    cmd_mplayer "3d" "/usr/local/void/product/octoprint/" || return
    append

    cmd_chrome "tt" "http://lolcathost/lolcathost" || return
    append

    cmd_chrome "ff" "https://map.freifunk-mwu.de/#/en/map/64700244607a" || return
    hc use_index +1

    printf "started up\n"
}

teardown() {
    cleanup
    relieve_me "$$"
    exit 0
}

monitoring() {
    while IFS=$'\t' read -ra entry; do
        case ${entry:0} in
            reload) teardown; exit ;;
        esac
    done < <(hc --idle)
}

cycler() {
    while sleep 90; do
        printf "proclist %s =>\n%s\n" "$0" "$(child_processes "$$")"
        printf "round %s => %s\n" "$0" "$((++CYCLE))"
        hc use_index +1
    done
}

trap teardown SIGINT SIGTERM SIGQUIT

startup || { (>&2 echo "something is wrong - can't start up"); exit 1; }
monitoring &
cycler &

while pgrep -P "$$" >/dev/null; do
    printf "proclist %s =>\n%s\n" "$0" "$(child_processes "$$")"
    wait;
done

printf "end of cycle\n"

