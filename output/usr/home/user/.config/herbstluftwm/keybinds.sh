#!/usr/local/bin/bash
# shellcheck source=common.sh
. ~/.config/herbstluftwm/common.sh

reset_keybinds
reset_tags

hc keybind $MOD-Shift-q quit
hc keybind $MOD-Shift-r reload
hc keybind $MOD-Shift-c close
hc keybind $MOD-Return spawn "${TERMINAL:-xterm}"

# basic movement
# focusing clients
hc keybind $MOD-Left  focus left
hc keybind $MOD-Down  focus down
hc keybind $MOD-Up    focus up
hc keybind $MOD-Right focus right
hc keybind $MOD-h     focus left
hc keybind $MOD-j     focus down
hc keybind $MOD-k     focus up
hc keybind $MOD-l     focus right

# moving clients
hc keybind $MOD-Shift-Left  shift left
hc keybind $MOD-Shift-Down  shift down
hc keybind $MOD-Shift-Up    shift up
hc keybind $MOD-Shift-Right shift right
hc keybind $MOD-Shift-h     shift left
hc keybind $MOD-Shift-j     shift down
hc keybind $MOD-Shift-k     shift up
hc keybind $MOD-Shift-l     shift right

# splitting frames
# create an empty frame at the specified direction
hc keybind $MOD-u       split   bottom  0.5
hc keybind $MOD-o       split   right   0.5
# let the current frame explode into subframes
hc keybind $MOD-Control-space split explode

# resizing frames
resizestep=0.05
hc keybind $MOD-Control-h       resize left +$resizestep
hc keybind $MOD-Control-j       resize down +$resizestep
hc keybind $MOD-Control-k       resize up +$resizestep
hc keybind $MOD-Control-l       resize right +$resizestep
hc keybind $MOD-Control-Left    resize left +$resizestep
hc keybind $MOD-Control-Down    resize down +$resizestep
hc keybind $MOD-Control-Up      resize up +$resizestep
hc keybind $MOD-Control-Right   resize right +$resizestep

# tags
default_tags
for _ in "${TAG_NAMES[@]}"; do
    tag_idx="$((idx++))"; tag_key="$((idx))"
    hc keybind "$MOD-$tag_key"  use_index $tag_idx
    hc keybind "$MOD-Shift-$tag_key"  use_index $tag_idx
done

# cycle through tags
hc keybind $MOD-period use_index +1 --skip-visible
hc keybind $MOD-comma  use_index -1 --skip-visible

# layouting
hc keybind $MOD-r remove
hc keybind $MOD-s floating toggle
hc keybind $MOD-f fullscreen toggle
hc keybind $MOD-p pseudotile toggle
# The following cycles through the available layouts within a frame, but skips
# layouts, if the layout change wouldn't affect the actual window positions.
# I.e. if there are two windows within a frame, the grid layout is skipped.
hc keybind $MOD-space                                                           \
            or , and . compare tags.focus.curframe_wcount = 2                   \
                     . cycle_layout +1 vertical horizontal max vertical grid    \
               , cycle_layout +1

# mouse
hc mouseunbind --all
hc mousebind $MOD-Button1 move
hc mousebind $MOD-Button2 zoom
hc mousebind $MOD-Button3 resize

# focus
hc keybind $MOD-BackSpace   cycle_monitor
hc keybind $MOD-Tab         cycle_all +1
hc keybind $MOD-Shift-Tab   cycle_all -1
hc keybind $MOD-c cycle
hc keybind $MOD-i jumpto urgent


printf "end of keybinds\n"
