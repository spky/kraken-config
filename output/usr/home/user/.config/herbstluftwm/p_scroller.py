#!/usr/bin/env python3
from time import sleep
from os import path
from sys import argv


def scroll(text, wdt=12):
    tln = len(text)
    for num, tx in enumerate(text):
        if num <= wdt:
            yield ''.join([' ' * (wdt - num), text[:num]])
        else:
            pln = num + wdt
            yield ''.join([text[num:pln], ' ' * (pln - tln)])
    yield ' ' * wdt


def deliver(line):
    queue = path.abspath(path.expanduser('~/.panel_queue'))
    if not path.exists(queue):
        return
    with open(queue, 'w') as que:
        que.write('message\t{}\n'.format(line))


def main():
    text = ' '.join(argv[1:])
    if not text:
        text = 'Frische Fleisch und Wurstwaren!!1!'

    for elem in scroll(text):
        deliver(elem)
        print(elem, end='\r')
        sleep(1)
    print()
    return True


if __name__ == '__main__':
    exit(not main())
