#!/usr/local/bin/bash
# shellcheck source=common.sh
. ~/.config/herbstluftwm/common.sh

geom=( $(get_geometry) )
geom[3]=$PANEL_HEIGHT
background=$(hc get frame_bg_normal_color)
foreground=$(hc get_attr theme.active.color)
halfground=$(hc get frame_bg_active_color)
urgent_col=$(hc get_attr theme.urgent.color )
accent_col=$(hc get frame_border_active_color)

remove_queue() { [ -f "$QUEUE" ] && rm "$QUEUE"; }
queue_put_double() {
    [ -p "$QUEUE" ] && printf "%s\t%s\t%s\n" "$1" "$2" "$3" >> "$QUEUE"
}
queue_put() { queue_put_double "$1" "$2" ""; }

teardown() {
    relieve_me "$$"
    remove_queue
    reset_padding
    exit
}


panel_clock() {
    while sleep .25; do
        queue_put_double "datetime" "$(date +"%Y-%m-%d")" "$(date +"%H:%M:%S")"
    done
}

panel_cpuload() {
    while sleep 9; do
        queue_put "cpuload" "$( awk '{print $1, $2, $3}' < "/compat/linux/proc/loadavg" )"
    done
}

panel_uptime() {
    while sleep 19; do
        upt=$(cat "/compat/linux/proc/uptime"); upt=${upt%%.*}
        day=$((upt/60/60/24))
        res=$( printf "%.2d:%.2d" $((upt/60/60%24)) $((upt/60%60)) )
        [ $day -gt 0 ] && res=$( printf "%d days %s" "$day" "$res" )
        queue_put "uptime" "$res"
    done
}

panel_musicpd() {
    mpc() { /usr/local/bin/mpc -q -h "lolcat@lolcathost" "$@"; }
    while IFS=$'\t' read -ra entry; do
        queue_put "music" "$(mpc current)"
    done < <(mpc idleloop)
}

panel_herbstclient() {
    make_tags() {
        IFS=$'\t' read -ra tags <<< "$(hc tag_status 0)"
        content=""
        for entry in "${tags[@]}"; do
            case ${entry:0:1} in
                '#') content+="^bg($foreground)^fg($background)" ;; # the tag is viewed on the specified MONITOR and it is focused.
                '+') content+="^bg($foreground)^fg($halfground)" ;; # the tag is viewed on the specified MONITOR, but this monitor is not focused.
                ':') content+="^bg($halfground)^fg($accent_col)" ;; # the tag is not empty
                '!') content+="^bg($urgent_col)^fg()"            ;; # the tag contains an urgent window
                  *) content+="^bg()^fg()"                       ;;
            esac
            content+=" ${entry:1} "
        done
        queue_put "tags" "$content"
    }

    make_tags
    while IFS=$'\t' read -ra entry; do
        case ${entry:0} in
            tag*) make_tags ;;
            focus_changed|window_title_changed)
                queue_put "windowname" "${entry[*]:2}" ;;
            quit_panel|reload)
                queue_put "_quit_" "" ;;
        esac
    done < <(hc --idle)
}

panel_draw() {
    {
        hc pad 0 $PANEL_HEIGHT "" "" ""
        content=( )
        meters=( " " "▁" "▂" "▃" "▄" "▅" "▆" "▇" "█" )
        equalizer() { rand() { printf "%d" "$(( RANDOM % ${#meters[@]} ))"; }
            printf "%s%s" "${meters[$(rand)]}" "${meters[$(rand)]}"
        }
        symbols=( "▖" "▄" "▗" "▐" "▝" "▀" "▘" "▌" )
        sym=0
        while : ; do
            [ -p "$QUEUE" ] && IFS=$'\t' read -ra entry < "$QUEUE"
            [ -z "$entry" ] && continue
            case ${entry:0} in
                _quit_)     teardown; exit ;;
                tags)       content[7]="${entry[*]:1}" ;;
                datetime)   content[6]="^fg($accent_col)${entry[1]} ^fg()${entry[2]}" ;;
                uptime)     content[5]="^fg($accent_col)up ^fg()${entry[1]}" ;;
                cpuload)    content[4]="^fg($accent_col)load ^fg()${entry[1]}" ;;
                music)      current_track="${entry[*]:1}"; cl_mus=320 ;;
                message)    content[2]="^fg($accent_col)☕ ^fg()${entry[*]:1}"; cl_msg=120 ;;
                windowname) content[1]="$(echo "${entry[*]:1}" | cut -c 1-23)" ;;
                *)          content[0]="⸘${entry[*]}‽"; cl_wtf=30 ;;
            esac
            [ ! -z "$current_track" ] && {
                content[3]="^fg($accent_col)♫ ^fg()$(equalizer) $(echo "$current_track" | cut -c 1-23)";
            }
            [ $((cl_mus--)) -lt 0 ] && content[3]=""
            [ $((cl_msg--)) -lt 0 ] && content[2]=""
            [ $((cl_wtf--)) -lt 0 ] && content[0]=""
            for elem in "${content[@]}"; do
                [ ! -z "$elem" ] && printf "^bg()^fg() %s^bg()^fg(%s) │" "$elem" "$urgent_col"
            done
            [ ${#symbols[@]} -le $((++sym)) ] && sym=0
            printf "^bg()^fg()%s\n" "${symbols[$sym]}"
        done
    } | dzen2 \
        -x "${geom[0]}" -y "${geom[1]}" -w "${geom[2]}" -h "${geom[3]}" \
        -ta r -fn "$DZEN_FONT" -bg "$background" -fg "$foreground"
}


trap teardown SIGINT SIGTERM SIGQUIT
remove_queue
[ ! -p "$QUEUE" ] && mkfifo "$QUEUE"

panel_clock &
printf "clock\t%s\n" "$!"
panel_cpuload &
printf "cpuload\t%s\n" "$!"
panel_uptime &
printf "uptime\t%s\n" "$!"
panel_musicpd &
printf "musicpd\t%s\n" "$!"
panel_herbstclient &
printf "herbstc\t%s\n" "$!"
panel_draw &
printf "draw\t%s\n" "$!"

printf "parent\t%s\n" "$$"

while pgrep -P "$$" >/dev/null; do
    printf "proclist %s =>\n%s\n" "$0" "$(child_processes "$$")"
    wait;
done

printf "end of panel\n"

