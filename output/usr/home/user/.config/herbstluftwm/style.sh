#!/usr/local/bin/bash
# shellcheck source=common.sh
. ~/.config/herbstluftwm/common.sh

# theme
hc attr theme.tiling.reset 1
hc attr theme.floating.reset 1

hc set frame_bg_normal_color '#002b36'
hc set frame_bg_active_color '#073642'
hc set frame_border_active_color '#268bd2'
hc set frame_border_normal_color '#839496'
hc attr theme.active.color '#eee8d5'
hc attr theme.normal.color '#839496'
hc attr theme.urgent.color '#cb4b16'
hc attr theme.floating.outer_color '#002b36'

hc set window_gap 0
hc set frame_border_width 2
hc set always_show_frame 0
hc set frame_bg_transparent 1
hc set frame_transparent_width 2
hc set frame_gap 2

hc attr theme.inner_width 0
hc attr theme.border_width 2
hc attr theme.floating.border_width 4
hc attr theme.floating.outer_width 2

hc set frame_padding 0
hc set smart_window_surroundings 0
hc set smart_frame_surroundings 1
hc set mouse_recenter_gap 0

default_tags

reset_padding
printf "end of style\n"
