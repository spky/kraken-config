#!/usr/local/bin/bash

hc() { herbstclient "$@"; }

# main modifier key
export MOD=Mod4
export QUEUE=$HOME/.panel_queue
export PANEL_HEIGHT=20
export DZEN_FONT="-*-source code pro-medium-*-*-*-16-*-*-*-*-*-*-*"
export DZEN_FONT_FULL="-*-source code pro-bold-*-*-*-128-*-*-*-*-*-*-*"
export TAG_NAMES=( "⁇" "⁉" "‼" )

reset_tags() {
    printf "reset tags\n"
    hc use_index 0
    IFS=$'\t' read -ra tags <<< "$(hc tag_status 0)"
    for entry in "${tags[@]}"; do
        hc use_index +1
        hc merge_tag "${entry:1}" "${tags[0]:1}" || true
    done
    hc rename "${tags[0]:1}" "+"
    printf "done\n"
}

reset_keybinds() {
    printf "reset keybinds\n"
    # remove all existing keybindings
    hc keyunbind --all
    # set common global keybinds
    hc keybind $MOD-Shift-Escape quit
    hc keybind $MOD-Escape spawn ~/.config/herbstluftwm/keybinds.sh
}

reset_padding() { hc pad 0 0 0 0 0; }

default_tags() {
    printf "setup default tags\n"
    hc rename "default" "${TAG_NAMES[0]}" || true
    hc rename "+" "${TAG_NAMES[0]}" || true
    for tag_name in "${TAG_NAMES[@]}"; do
        hc add "$tag_name"
    done
}

get_geometry() {
    local geom geom=( $(hc monitor_rect "0") )
    [ -z "${geom[*]}" ] && (>&2 echo "Invalid monitor")
    echo "${geom[@]}"
}

child_processes() {
    procs=$(pgrep -P "$1");
    [ ! -z "$procs" ] && {
        printf "%s\n" "$procs"
        for proc in $procs; do child_processes "$proc"; done
    }
}

relieve_me() {
    for pid in $(child_processes "$$"); do
        printf "killing\t%s\n" "$pid"
        kill "$pid" || true
    done
}
