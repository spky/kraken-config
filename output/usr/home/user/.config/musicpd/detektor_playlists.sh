#!/bin/sh
TARGET="$HOME/.config/musicpd/playlists"
BASE="https://detektor.fm/"
curl -Lo "$TARGET/detektor_fm-wort.m3u" "$BASE/detektor_fm-wort.m3u"
curl -Lo "$TARGET/detektor_fm-musik.m3u" "$BASE/detektor_fm-musik.m3u"
