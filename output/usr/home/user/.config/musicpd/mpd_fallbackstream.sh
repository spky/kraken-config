#!/bin/sh

STREAM="${1-http://ice1.somafm.com/defcon-128-aac}"
mpc() { /usr/local/bin/mpc -h "lolcat@localhost" "$@"; }
[ -z "$(mpc current)" ] && mpc -q add "$STREAM"
mpc -q enable 1 > /dev/null
mpc -q play
