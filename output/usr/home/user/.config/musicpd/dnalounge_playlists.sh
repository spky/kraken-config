#!/bin/sh
TARGET="$HOME/.config/musicpd/playlists"
BASE="http://cerebrum.dnalounge.com:8000/live"
curl -o "$TARGET/dnalounge_mainroom.m3u" "$BASE/128.m3u"
curl -o "$TARGET/dnalounge_upstairs.m3u" "$BASE/128b.m3u"
curl -o "$TARGET/dnalounge_radio.m3u" "$BASE/radio.m3u"
