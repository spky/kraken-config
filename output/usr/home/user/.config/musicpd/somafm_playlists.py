#!/usr/bin/env python3

from os import path, makedirs
from xml.etree import ElementTree
from urllib.request import urlopen
from argparse import ArgumentParser


def arguments():
    parser = ArgumentParser(__file__)
    parser.add_argument(
        '-u', '--url', help='somafm channels api url',
        default='https://api.somafm.com/channels.xml'
    )
    parser.add_argument(
        '-o', '--out', help='output directory',
        default=path.join(path.abspath(path.dirname(__file__)), 'playlists')
    )
    return parser.parse_args()


def fetch(url):
    with urlopen(url) as req:
        if req.status != 200:
            return print('error getting {}'.format(url))
        return req.read()


def parse(data):
    for channel in ElementTree.fromstring(data):
        yield (channel.attrib['id'], [
            el.text for el in channel if el.attrib.get('format')
        ])


def extract(out, key, elems):
    result = []
    mname = 'somafm_{}.m3u'.format(key)
    for elem in elems:
        pname = 'somafm_{}'.format(path.basename(elem))
        playlist = fetch(elem).decode()
        with open(path.join(out, pname), 'w') as pls:
            pls.write(playlist)
        for line in playlist.splitlines():
            if line.startswith('File'):
                result.append(line.split('=')[-1])
    with open(path.join(out, mname), 'w') as m3u:
        m3u.write('\n'.join(sorted(result)))


def main():
    args = arguments()
    raw_data = fetch(args.url)
    if not path.exists(args.out):
        makedirs(args.out)
    for key, elems in parse(raw_data):
        extract(args.out, key, elems)
        print(key, '', end=' ')
    print()
    return True


if __name__ == '__main__':
    exit(not main())
