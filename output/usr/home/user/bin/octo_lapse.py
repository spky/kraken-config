#!/usr/bin/env python3
from argparse import ArgumentParser
from os import makedirs, path, stat
from sys import stderr

from requests import get
from requests.exceptions import RequestException


def message(*text, err=False):
    stderr.write((
        'ERROR' if err else 'NOTE'
    ) + ':\t' + ' '.join(str(txt) for txt in text) + '\n')
    return (not err)


def get_list(args):
    try:
        req = get(
            'http://{base}/api/timelapse?apikey={key}'.format(**vars(args))
        )
    except RequestException as exc:
        return message('Request', exc, err=True)

    if req.status_code != 200:
        return message('Status', vars(req), err=True)

    for item in req.json().get('files', []):
        url = item.get('url')
        if url:
            yield (
                item.get('bytes', 0),
                path.join(path.abspath(args.root), path.basename(url)),
                'http://{base}{url}'.format(**vars(args), url=url),
            )


def get_file(size, local, remote):
    parent = path.dirname(local)
    if not path.exists(parent) and not path.isdir(parent):
        message('Makedir', parent)
        makedirs(parent)
    if path.exists(local) and stat(local).st_size == size:
        return message('Skipping', local, err=False)
    req = get(remote, stream=True)
    if req.status_code != 200:
        return message('Retrieve', vars(req), err=True)
    with open(local, 'wb') as fsobj:
        message('Loading', local)
        for chunk in req:
            fsobj.write(chunk)
        return True


def arguments():
    parser = ArgumentParser(__file__)
    parser.add_argument('-b', '--base', action='store', default='octopi.lan')
    parser.add_argument('-k', '--key', action='store', required=True)
    parser.add_argument('-r', '--root', action='store', default=path.join(
        path.abspath(path.dirname(__file__)), 'root'
    ))
    return parser.parse_args()


def main():
    return all(get_file(*fl) for fl in get_list(arguments()))


if __name__ == '__main__':
    exit(not main())

