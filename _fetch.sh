#!/bin/sh

THIS_DIR=$(cd "$(dirname "$0")" || exit 3; pwd)
INPUT=${INPUT-"$THIS_DIR/input.txt"}
OUTPUT=${OUTPUT-"$THIS_DIR/output"}
OWNER=${OWNER-"user"}

if [ "$(id -un)" != 'root' ]; then
    printf ":: %s\n" "sorry, must be run as root"
    exit 2
fi
if [ ! -f "$INPUT" ]; then
    printf ":: %s '%s'\n" "sorry, no input file present at" "$INPUT"
    exit 1
fi
if [ ! -d "$OUTPUT" ]; then
    printf ":: %s '%s'\n" "create output folder at" "$OUTPUT"
    mkdir -vp "$OUTPUT"
fi

while read -r elem; do
    if case $elem in /*) ;; *) false;; esac; then
        info="$(file -bi0 "$elem")"
        if case $info in text*) ;; *) false;; esac; then
            target="$OUTPUT$elem"
            case $elem in *.git/config)
                target=$(echo "$target" | sed 's/.git\/config$/.git_config/g') ;;
            esac;
            parent="$(dirname "$target")"
            if [ ! -d "$parent" ]; then
                printf ":: %s '%s'\n" "create folder at" "$parent"
                mkdir -vp "$parent"
            fi
            [ -f "$target" ] && diff "$target" "$elem"
            cp -p "$elem" "$target" && chown "$OWNER" "$target"
            printf "^^ '%s'\n" "$elem"
        else
            printf "\n?? '%s'\n?? %s\n" "$elem" "this is not text"
        fi
    else
            printf "\n-- '%s'\n-- %s\n" "$elem" "this is not a file"
    fi
done < "$INPUT"

printf "\n!! %s\n" "done"
exit 0
