#!/bin/sh

WIKIUSER="${WIKIUSER-"$1"}"
WIKIPASS="${WIKIPASS-"$2"}"
[ -z "$WIKIUSER" ] && { (>&2 echo "WIKIUSER missing"); exit 1; }
[ -z "$WIKIPASS" ] && { (>&2 echo "WIKIPASS missing"); exit 1; }

FILENAME="${3-"kraken.pdf"}"
[ ! -f "$FILENAME" ] && { (>&2 echo "no such FILENAME"); exit 1; }

PAGEID="${4-"12484792"}"
FILEID="${5-"att12484997"}"
WIKIBASE="${6-"https://wiki.cccmz.de"}"


URL="$WIKIBASE/rest/api/content/$PAGEID/child/attachment/$FILEID/data"
printf "\tpushing '%s' to ->\n'%s'\n\t<-\n" "$FILENAME" "$URL"

# echo "
curl \
    -D- \
    -u "$WIKIUSER:$WIKIPASS" \
    -X POST -H "X-Atlassian-Token: nocheck" \
    -F "file=@$FILENAME" -F "minorEdit=true" \
    "$URL"
# "
