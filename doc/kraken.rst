.. footer:: |info|
.. header:: |info|
.. include:: <isonum.txt>
.. title:: kraken - rtfm
.. |date| date:: %Y-%m-%d %H:%M:%S %z
.. |info| replace:: kraken |middot| rtfm |middot| |date|
.. |pass| replace:: lolcat
.. |user| replace:: user

==================
Welcome to kraken!
==================

This machine may be tiny, but it does a lot of stuff..

* It plays Music
    * see `Music Player Daemon`_

* You can share files (e.g. upload new music :)
    * see Samba_

* Does Web stuff
    * see Nginx_

* Displays stuff on the street
    * see Monitor_

* Runs some `self made Projects <Projects>`_
    * BayPack_
    * `Plenarsaal Bot`_
    * Teletyper_


.. admonition:: Do not bite the hand which feeds you

    I am asking politely:

    - not to break, hack, or destroy this machine
        (you don't have to, if you ask nicely you may have all passwords..)
    - help me to find and fix bugs in the configuration
    - to notify me if something is wrong
    - and to stay awesome

    Thanks!

    .. pull-quote:: @spky


Configuration, scripts, anything important is stored inside some repository:

    * https://bitbucket.org/spky/bsd-config

----

=====
Samba
=====

.. |smb-baypack|    replace:: **product** |bsol| ``baypack``
.. |smb-teletyper|  replace:: **product** |bsol| ``teletyper``
.. |smb-tty-photos| replace:: **product** |bsol| ``teletyper/photos``
.. |smb-tty-videos| replace:: **product** |bsol| ``teletyper/videos``

You may share files via samba on kraken.

=========== ==== ==== =======================
share       any  user description
=========== ==== ==== =======================
**temp**    *rw* *rw* quickly share something
**product** *r-* *r-* view generated files
**music**   *--* *rw* `Music Player Daemon`_
=========== ==== ==== =======================

.. topic:: **temp**

    This share is intended to quickly transfer (smallish) files between two
    devices..

    Like the name suggests files are stored on ``/tmp``.

    Some script in the crontab removes all old files after some time..

.. topic:: **product**

    This is some read only share for anyone.

    Here one can obtain files which are generated on/by kraken.

    * BayPack_
        * |smb-baypack|
    * Teletyper_
        * |smb-teletyper|

.. topic:: **music**

    This is the music folder of `Music Player Daemon`_.

    To connect use these credentials:

        :username: |user|
        :password: |pass|

    Please upload only your finest tunes and make sure they are

    * in a decent quality
    * properly tagged [#picard]_
    * correctly named and in the right folder::

        %ARTIST/%ALBUM/%NN-%TITLE.%EXT

.. [#picard] I suggest Musicbrainz Picard: https://picard.musicbrainz.org

----

===================
Music Player Daemon
===================

.. |mpd-mpd-webgui| replace:: http://kraken.local:8080
.. |mpd-mpd-stream| replace:: http://kraken.local:6632

There is a `music player daemon <https://www.musicpd.org/>`_ running on kraken.

Anonymous users may see and add items to the playlist.
To edit and control use this:

    :password: |pass|
    :port:  6600 (default)

Although I explicitly compiled in the support for auto refreshing music,
it does not work :(

So the crontab takes care of this, if you've just added new music, just
wait a little..

----------
MPD Client
----------

Searching good clients is tough..

I am using / this setup is tested with:

    * Cantata
        * QT5
        * https://github.com/CDrummond/cantata
        * very good - absolute recommendation!
    * Theremin
        * OSX only
        * https://github.com/TheStalwart/Theremin
        * seems dead, even the fork of it :(
    * ncmpcpp
        * ncurses
        * https://rybczak.net/ncmpcpp/
        * I can never remember that freaking name..
    * mpc
        * *-*
        * https://www.musicpd.org/clients/mpc/
        * The 'official' one, I use it mostly for scripting.


.. topic:: But wait, there is more!

    Kraken runs `ympd <https://www.ympd.org/>`_ |rarr| MPD Web GUI !!1!

    You can either use it directly or through Nginx_:

    * |mpd-mpd-webgui|
    * |www-mpd-webgui|

----------
MPD Stream
----------

Music played there is also transmitted via http.
To be compatible with almost anything this is some 320 kBit/s mp3 stream.

You can either tune in to it directly or through Nginx_:

    * |mpd-mpd-stream|
    * |www-mpd-stream| (|rarr| should not break down if MPD stops playing..)


If the player needs some explicit filename you may append anything
to the url (works on both).

.. parsed-literal::

    |mpd-mpd-stream|/omg/wtf/bbq.mp3
    |www-mpd-stream|/lol/rofl/xD.mp3

.. note::

    **There is another cron script to keep the stream alive!**

    It checks the current state and presses play.
    If the playlist is empty, some webstream to
    `DEF CON Radio <http://somafm.com/defcon/>`_ is added first.

-------------
MPD Playlists
-------------

There are two scripts running via cron, to add some webstreams as playlists.
You may edit the generated playlists, but changes will be lost ;)

    :soma fm:    http://somafm.com/listen
    :dna lounge: https://www.dnalounge.com/webcast

----

=====
Nginx
=====

.. |www-root|       replace:: http://kraken.local
.. |www-baypack|    replace:: http://kraken.local/baypack
.. |www-teletyper|  replace:: http://kraken.local/lolcathost
.. |www-tty-photos| replace:: http://kraken.local/lolcathost/photos
.. |www-tty-videos| replace:: http://kraken.local/lolcathost/videos
.. |www-mpd-stream| replace:: http://kraken.local/stream
.. |www-mpd-webgui| replace:: http://kraken.local/ympd

Nothing really special here, fancy indexing is enabled, so look for yourself.

Mostly it is here to provide these files (for you and the Monitor_):

    * |www-baypack|
    * |www-teletyper|
    * |www-tty-photos|
    * |www-tty-videos|

The server also redirects some traffic (for `MPD <Music Player Daemon>`_):

.. parsed-literal::

    * |www-mpd-stream| |rarr| |mpd-mpd-stream|
    * |www-mpd-webgui| |rarr| |mpd-mpd-webgui|

----

=======
Monitor
=======

.. pull-quote:: Main screen turn on! We get signal!

To amuse and inform people, some stuff is shown publicly.

After boot, the user |user| gets logged in automatically to
start some X-Server.

As window manager `herbstluftwm <http://herbstluftwm.org>`_ is running,
with a lot of self-made scripts.

It features (using `dzen <https://github.com/robm/dzen>`_):

    * a panel on top
        * clock, system info, subtle messages, currently playing, ...
        * multi-threaded real-time shell script.. |rarr| fuck yeah! |larr|
    * full screen messages
        * current time every quarter hour
    * automatic cycling through different things to display
        * BayPack_ output
        * Images from Teletyper_
        * our Freifunk-Node `on the Map <https://map.wiesbaden.freifunk.net>`_
        * whatever comes in mind..
            * meatspin, lemonparty, goatse

----

========
Projects
========

-------
BayPack
-------

This is some crawler for our PayBack |trade| Account.
It stores the |deg| Points and generates some files:

============= ================ =========================
short content full content     description
============= ================ =========================
export.csv    export_full.csv  the 80s have called...
export.json   export_full.json guess what
feed.atom     feed_full.atom   subscribe in your reader!
index.html    index_full.html  view table in browser
*-*           today.json       'caches' incomplete days
============= ================ =========================

Please do funny stuff with the generated files!
Get the Files via Samba_ or `http <Nginx>`_

    * |smb-baypack|
    * |www-baypack|

.. hint::

    :source: http://bitbucket.org/spky/baypack
    :wiki: https://wiki.cccmz.de/display/PRO/Payback


--------------
Plenarsaal Bot
--------------

Checks our wiki, and may or may not send a mail to everyone..

.. hint::

    :source: https://bitbucket.org/spky/plenarsaal


---------
Teletyper
---------

This is our Telegram bot. It collects photos and videos and uploads them.

:tumblr: https://cccwi.tumblr.com
:vimeo: https://vimeo.com/cccwi

In addition the files are stored locally and
are shown as a slide show on the `main screen <Monitor>`_.

Get the Files via Samba_ or `http <Nginx>`_

* |smb-tty-photos|
* |smb-tty-videos|
* |www-tty-photos|
* |www-tty-videos|

.. hint::

    :source: https://github.com/spookey/teletyper
