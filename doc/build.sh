#!/bin/sh
THIS_DIR=$(cd "$(dirname "$0")" || exit 1; pwd)
build() {
    res=$(which "$1")
    [ -z "$res" ] && res=$(which "${1}.py")
    $res < "$THIS_DIR/kraken.rst"
}
build "rst2html" > "$THIS_DIR/kraken.html"
build "rst2pdf" > "$THIS_DIR/kraken.pdf"
