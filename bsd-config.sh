#!/bin/sh

THIS_DIR=$(cd "$(dirname "$0")" || exit 3; pwd)
THAT_FILE="$THIS_DIR/_fetch.sh"
INPUT=${INPUT-"$THIS_DIR/input.txt"}

if [ -z "$*" ]; then
    $THAT_FILE
else
    for elem in "$@"; do
        realpath "$elem" >> "$INPUT";
    done
    elems=$(sort < "$INPUT" | uniq)
    echo "$elems" > "$INPUT"
fi
